# Utility Infrastructure

This repo contains the Terraform code to provision and manage AWS hosted utitilies environment.

## List of utilities (Mar 12, 2021):

- GitLab runner
- Bitwarden

## AWS facts

- All components are currently provisioned inside `us-east-1`
- The GitLab runner is provisioned manually in a module because chicken and the egg

## Tool version

- Terraform 0.14.8
- AWS 2.1.29
