terraform {
  backend "s3" {
    bucket = "datsmy.name.tfstate"
    key = "terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  profile = "default"
  region = "us-east-1"
}
