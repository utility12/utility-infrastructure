resource "aws_vpc" "main" {
  cidr_block = "11.0.0.0/16"
}

#resource "aws_subnet" "all_subnets" {
#  vpc_id = aws_vpc.main.id
#  cidr_block = "11.0.0.0/24"
#  availability_zone = "us-east-1a"
#
#  tags = {
#    Name = "Management"
#  }
#}
#
#resource "aws_subnet" "public" {
#  vpc_id = aws_vpc.main.id
#  cidr_block = "11.0.1.0/24"
#  availability_zone = "us-east-1a"
#
#  tags = {
#    Name = "private"
#  }
#}
#
#resource "aws_internet_gateway" "main" {
#  vpc_id = aws_vpc.main.id
#
#  tags = {
#    Name = "main"
#  }
#}
#
#resource "aws_route_table" "public_route_table" {
#  vpc_id = aws_vpc.main.id
#
#  route {
#    cidr_block = "0.0.0.0/0"
#    gateway_id = aws_internet_gateway.main.id
#  }
#
#  tags = {
#    Name = "main"
#  }
#}
#
#resource "aws_route_table_association" "public_subnet_rta" {
#  subnet_id = aws_subnet.public.id
#  route_table_id = aws_route_table.public_route_table.id
#}
#
#resource "aws_route_table" "private_route_table" {
#  vpc_id = aws_vpc.main.id
#
#  tags = {
#    Name = "main"
#  }
#}
#
#resource "aws_route_table_association" "private_subnet_rta" {
#  subnet_id = aws_subnet.private.id
#  route_table_id = aws_route_table.private_route_table.id
#}
## TODO:  create NAT in public, route private => NAT
